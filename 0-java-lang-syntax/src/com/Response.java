package com;

import java.util.ArrayList;
import java.util.List;

public class Response extends BasicResponse {
	private String name;
	private String age;
	private List<Response.Accounts> accounts;
	private Response.Accounts resaccts;
	private List<Account> accts;
	
	public List<Account> getAccts() {
		if(accts == null) {
			accts = new ArrayList<Account>();
		}
		return accts;
	}
	public void setAccts(List<Account> accts) {
		this.accts = accts;
	}
	public Response.Accounts getResaccts() {
		return new Accounts();
	}
	public void setResaccts(Response.Accounts resaccts) {
		this.resaccts = resaccts;
	}
	public void setAccounts(List accounts) {
		this.accounts = accounts;
	}
	public List getAccounts() {
		return accounts;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	class Accounts {
		
		
		public List<Account> getAccounts(){
			if(accts == null) {
				accts = new ArrayList();
			}
			return accts;
		}
		
		
		
	}
}
