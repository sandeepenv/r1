package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Person{
	final String name="person";
	final ArrayList<String> items=new ArrayList<>();
}
public class Ex {

	Response res;
	List<A> list;

	public Ex() {
		this.res = new Response();
		this.list = new ArrayList<A>();
		A a = new A();
		a.setA1("123");
		a.setA2("abc");
		list.add(a);
		
		a = new A();		
		a.setA1("123");
		a.setA2("abc");
		list.add(a);
		
		a = new A();
		a.setA1("123");
		a.setA2("abc");
		list.add(a);
		
		a = new A();
		a.setA1("1234");
		a.setA2("abcd");
		list.add(a);
		
		a = new A();
		a.setA1("12345");
		a.setA2("abcde");
		list.add(a);
		
		a = new A();
		a.setA1("123456");
		a.setA2("abcdef");
		list.add(a);
		
		a = new A();
		a.setA1("1234567");
		a.setA2("abcdefg");
		list.add(a);
	}

	public static void main(String[] args) {

		Ex ex = new Ex();				
		ex.display();


	}

	private void display() {
		res = constructResponse(this.res);
		Utility.display(res);
	}

	public  Response constructResponse(Response res){
		Iterator<A> itr = list.iterator();
		do{
			res = Utility.constructResponse(itr.next(),res);
		}while(itr.hasNext());
		return res;
	}

}
