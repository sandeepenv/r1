package com;

public class A extends S{
	private String a1;
	private String a2;
	
	public A() {
		
		System.out.println(" A hashcode:: "+this.hashCode()); 
		
		setD1("10");
		setD2("20");
		
		setA1("30");
		setA2("40");
	}
	
	public String getA1() {
		return a1;
	}
	public void setA1(String a1) {
		this.a1 = a1;
	}
	public String getA2() {
		return a2;
	}
	public void setA2(String a2) {
		this.a2 = a2;
	}
}

class B extends A {
	
	public B() {
		
		System.out.println(" B hashcode:: "+this.hashCode());
		
		setA1("50");
		setA2("60");
	}
	
	private String a3;
	private String a4;
	public String getA3() {
		return a3;
	}
	public void setA3(String a3) {
		this.a3 = a3;
	}
	public String getA4() {
		return a4;
	}
	public void setA4(String a4) {
		this.a4 = a4;
	}
	
}